local overrides = require "custom.configs.overrides"

---@type NvPluginSpec[]
local plugins = {

  -- Custom plugins
  { "echasnovski/mini.map", config = true, event = "BufRead" },

  { "tpope/vim-fugitive", lazy = false },

  { "ellisonleao/glow.nvim", config = true, event = "BufRead *.md", cmd = "Glow" },

  { "max397574/better-escape.nvim", event = "InsertEnter", config = true },

  {
    "zbirenbaum/copilot.lua",
    cmd = "Copilot",
    config = function()
      require "custom.configs.copilotconfig"
    end,
  },

  {
    "akinsho/git-conflict.nvim",
    version = "^1.1.2",
    keys = "<leader>gc",
    config = function()
      require "custom.configs.git-conflict"
    end,
  },

  -- Override plugin definition options

  {
    "neovim/nvim-lspconfig",
    dependencies = {
      -- format & linting
      {
        "jose-elias-alvarez/null-ls.nvim",
        config = function()
          require "custom.configs.null-ls"
        end,
      },
    },
    config = function()
      require "plugins.configs.lspconfig"
      require "custom.configs.lspconfig"
    end, -- Override to setup mason-lspconfig
  },

  -- overrde plugin configs
  {
    "nvim-treesitter/nvim-treesitter",
    opts = overrides.treesitter,
  },

  {
    "nvim-tree/nvim-tree.lua",
    opts = overrides.nvimtree,
  },

  {
    "williamboman/mason.nvim",
    opts = overrides.mason,
  },

  -- To make a plugin not be loaded
  -- {
  --   "NvChad/nvim-colorizer.lua",
  --   enabled = false
  -- },

  -- Uncomment if you want to re-enable which-key
  -- {
  --   "folke/which-key.nvim",
  --   enabled = true,
  -- },
}

return plugins
