---@type MappingsTable
local M = {}

M.general = {
  n = {
    [";"] = { ":", "enter command mode", opts = { nowait = true } },
    ["<leader>tt"] = {
      function ()
        require("base46").toggle_transparency()
      end
    },
    ["<leader>qq"] = {
      function ()
        require("nvchad_ui.tabufline").closeAllBufs()
      end
    },
    ["<leader>dd"] = {
      ":lua vim.diagnostic.disable()<CR>"
    },
    ["<leader>md"] = {
      ":Glow<CR>"
    },
    ["<leader>co"] = {
      ":Copilot enable<CR>"
    },

    ["<leader>mt"] = {
        function ()
          require("mini.map").toggle()
        end
    },
    ["<leader>mf"] = {
        function ()
          require("mini.map").toggle_focus()
        end
    },
  },
}

-- more keybinds!

return M
