local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities

local lspconfig = require "lspconfig"

-- if you just want default config for the servers then put them in a table
local servers = { "clangd", "cmake", "bashls",  "pylsp", "gopls" }

for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

--
lspconfig.texlab.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  -- Set zathura as the viewer
  settings = {
    texlab = {
      build = {
        executable = "latexmk",
        args = { "-pdf","-pvc"},
        -- Enable on save build
        onSave = true,
        -- forwardSearchAfter = true,
        -- forwardSearch = {
        --     executable = "zathura",
        --     args = { "--synctex-forward", "%l:1:%f", "%p" },
        -- },
      },
      forwardSearch = {
        executable = "zathura",
        args = { "--synctex-forward", "%l:1:%f", "%p" },
      },
    },
  },
}
